package ScreenShot;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
@Listeners(Third.class)
public class Second extends First {
	@BeforeMethod
	  public void beforeClass() {
		initialize();
		
	  }
	@Test(priority = 0)
	public void bothvalueblank() {
		
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		String actual_Fname=driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
		String expact_Fname="Requed";
		String a=driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
		String b="Required";
		Assert.assertEquals(actual_Fname, expact_Fname);
		Assert.assertEquals(a, b);
  
  
  }
  
  @Test(priority = 1)
  public void EmailBlank() {
  
	  driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("1234568");
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		String actual_Fname=driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
		String expact_Fname="Required";
		Assert.assertEquals(actual_Fname, expact_Fname);
  
  
  }
  
  
  @Test(priority = 2)
  public void Passwordblank() throws InterruptedException {
	  
	 
  
	  driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("akakkssskkd@gmail.com");
	  driver.findElement(By.xpath("//*[@id=\"Password\"]")).clear();
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
	  //Thread.sleep(5000);
	  String actual_Fname=driver.findElement(By.xpath("(//span[@title='Required'])[2]")).getText();
	  String expected_Fname="Reqired";
	  //Thread.sleep(5000);
	  Assert.assertEquals(actual_Fname, expected_Fname);
  
  
  }
  
  
  

  @AfterMethod
  public void afterClass() {
	  driver.quit();
  
  }

}
