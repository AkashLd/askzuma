package TestNGPCKG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class SC_ChangeprofileBC extends SC_New_Login {

	  @BeforeTest
	  public void beforeTest() {
		  
	  }
	
//	@Test
//  public void f() {
//  }

	  @Test(priority = 14)
	  public void profileallvalueblank() throws InterruptedException {
		  Thread.sleep(5000);
		  //driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a")).click();
		  Actions act = new Actions(driver);
		  WebElement d = driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
		  act.moveToElement(d).perform();
		  driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[4]/a")).click();
		  Thread.sleep(5000); 
		  driver.findElement(By.xpath("//input[@id='firstname']")).clear();
		  driver.findElement(By.xpath("//input[@id='lastname']")).clear();
		  driver.findElement(By.xpath("//input[@id='phone']")).clear();
		  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
		  String a = driver.findElement(By.xpath("//*[@id=\"checkBlank1\"]/span")).getText();
		  String b = "First name cannot be blank";
		  Assert.assertEquals(a, b);
	  
	  
	  }
	  
	  @Test(priority = 15)
	  public void validfirstname() {
		  
		  driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("AL");
		  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
		  String a = driver.findElement(By.xpath("//*[@id=\"checkBlank2\"]")).getText();
		  String b = "Last name cannot be blank";
		  Assert.assertEquals(a, b);
		  
	  }
	  @Test(priority = 16)
	  public void Mobilenumberblank() {
		  
		  driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("D");
		  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
		  String a = driver.findElement(By.xpath("//*[@id=\"checkBlank3\"]/span")).getText();
		  String b = "Mobile number cannot be blank";
		  Assert.assertEquals(a, b);
		  
	  }
	  @Test(priority = 17)
	  public void profileAllvalue() {
	  
		  driver.findElement(By.xpath("//input[@id='phone']")).sendKeys("8160828601");
		  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  
	  }
	  @Test(priority = 18)
	  public void updationmessage() {
		  String a = driver.findElement(By.xpath("//*[@id=\"notification_85214fdf7d646b0d64eeb251383996d0\"]/div/div[2]")).getText();
		  String b = "Profile has been updated successfully.";
		  Assert.assertEquals(a, b);
	  }
  @AfterTest
  public void afterTest() {
  }

}
