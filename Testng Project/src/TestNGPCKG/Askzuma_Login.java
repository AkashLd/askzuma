package TestNGPCKG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Askzuma_Login {
	public WebDriver driver; 
	
	@BeforeTest
	  public void beforeTest() {
		  
		 
		  System.setProperty("webdriver.chrome.driver", "C:\\Users\\sony\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		  driver = new ChromeDriver();
		  driver.get("https://askzuma.com/");
		  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
		   
	  
	  }

	
	@Test(priority = 0)
	public void bothvalueblank() {
		
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		String actual_Fname=driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
		String expact_Fname="Required";
		String a=driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
		String b="Required";
		Assert.assertEquals(actual_Fname, expact_Fname);
		Assert.assertEquals(a, b);
  
  
  }
  
  @Test(priority = 1)
  public void EmailBlank() {
  
	  driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("1234568");
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		String actual_Fname=driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
		String expact_Fname="Required";
		Assert.assertEquals(actual_Fname, expact_Fname);
  
  
  }
  
  
  @Test(priority = 2)
  public void Passwordblank() throws InterruptedException {
	  
	 
  
	  driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("akakkssskkd@gmail.com");
	  driver.findElement(By.xpath("//*[@id=\"Password\"]")).clear();
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
	  //Thread.sleep(5000);
	  String actual_Fname=driver.findElement(By.xpath("(//span[@title='Required'])[2]")).getText();
	  String expected_Fname="Required";
	  //Thread.sleep(5000);
	  Assert.assertEquals(actual_Fname, expected_Fname);
  
  
  }
  
  @Test(priority = 3)
  public void login() throws InterruptedException {
  
  
  driver.findElement(By.xpath("//*[@id=\"Email\"]")).clear();
  driver.findElement(By.xpath("//*[@id=\"Password\"]")).clear();
  driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("ankur1.manish@gmail.com");
  driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("12345678");
  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
  Thread.sleep(5000);
  
  }
  
  @Test(priority = 4)
  public void signout() {
  
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/a")).click();
	  
	  
  }
  
  
  
  @AfterTest
  public void afterTest() {
	  
	  //driver.close();
  
  }

}
