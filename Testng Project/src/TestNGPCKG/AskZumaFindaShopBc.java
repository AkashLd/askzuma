package TestNGPCKG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class AskZumaFindaShopBc extends AskZumaSigninBC {
	@BeforeTest
	  public void beforeTest() {
	  }
	@Test(priority = 7)
  public void Search() throws InterruptedException {
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//a[text()='Find a shop'])[2]")).click();
		driver.findElement(By.xpath("//input[@id='tbLocation']")).sendKeys("Canada");
		driver.findElement(By.xpath("//button[@id='btnSearchShops']")).click();
  }
	@Test(priority = 8)
	public void LocationText() throws InterruptedException {
		Thread.sleep(5000);
		String a = driver.findElement(By.xpath("//a[text()='C P Auto Body']")).getText();
		String b = "C P Auto Body";
		Assert.assertEquals(a, b);
		System.out.println(a);
		
	}
  

  @AfterTest
  public void afterTest() {
  }

}
