package TestNGPCKG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;

public class addtocartBC extends SC_ChangepasswordBC{
	  @BeforeTest
	  public void beforeTest() {
	  }

	@Test(priority = 25)
  public void AddtoCart() throws InterruptedException {
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[text()='Home']")).click();
		Thread.sleep(5000);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("scrollBy(0,700)");
		Thread.sleep(5000);
		driver.findElement(By.id("127779866")).click();
		 ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		    driver.switchTo().window(tabs2.get(1));
		JavascriptExecutor js1 = (JavascriptExecutor)driver;
		js1.executeScript("scrollBy(0,500)");
		driver.findElement(By.xpath("//*[@id=\"add_cart\"]")).click();
		Thread.sleep(5000);
		Actions ac = new Actions(driver);
		WebElement f = driver.findElement(By.xpath("//a[@class='cart_ic']"));
		ac.moveToElement(f).perform();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//a[@class='btn orange-white btn_effect']")).click();
		//driver.findElement(By.xpath("//a[@class='minus']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//a[@class='remove']")).click();
		//driver.switchTo().alert();
		//Thread.sleep(5000);
		driver.findElement(By.xpath("//a[@class='prod-remove']")).click();
		driver.switchTo().window(tabs2.get(1)).close();
		Thread.sleep(5000);
		driver.quit();
  }

  @AfterTest
  public void afterTest() {
  }

}
