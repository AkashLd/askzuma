package TestNGPCKG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class SC_ChangepasswordBC extends SC_ChangeprofileBC {
	@BeforeTest
	  public void beforeTest() {
	  }
//	@Test
//  public void f() {
//  }
	@Test(priority = 19)
	  public void passwordallblank() throws InterruptedException {
			Thread.sleep(5000);
			driver.findElement(By.xpath("//a[text() = 'Change Password']")).click();
			driver.findElement(By.xpath("//input[@id= 'save_profile_but']")).click();
			String a = driver.findElement(By.xpath("//*[text()='Current password cannot be empty']")).getText();
			String b = "Current password cannot be empty";
			Assert.assertEquals(a, b);
			
		
		
		}
		@Test(priority = 21)
		  public void Cp() {
			driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("AV3iw1");
			driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
			String a = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]/span")).getText();
			String b = "New password cannot be empty";
			Assert.assertEquals(a, b);
		  }
		@Test(priority = 22)
		  public void invalidnewpassword() {
			driver.findElement(By.xpath("//input[@id= 'password1']")).sendKeys("123456");
			driver.findElement(By.xpath("//input[@id= 'save_profile_but']")).click();
			String a = driver.findElement(By.xpath("//div[@id= 'password1Blank']")).getText();
			String b = "Password must be alphanumeric";
			Assert.assertEquals(a, b);
		
		
		}
		@Test(priority = 23) 
		  public void blankretypepassword() {
			
			driver.findElement(By.xpath("//input[@id= 'password1']")).clear();
			driver.findElement(By.xpath("//input[@id= 'password1']")).sendKeys("A123456");
			driver.findElement(By.xpath("//input[@id= 'save_profile_but']")).click();
			String a = driver.findElement(By.xpath("//*[text()= 'Confirm password cannot be empty']")).getText();
			String b = "Confirm password cannot be empty";
			Assert.assertEquals(a, b);
		
		
		
		}
		@Test(priority = 24)
		  public void mismatchpassword() {
			driver.findElement(By.xpath("//*[@id= 'password2']")).sendKeys("654321A");
			driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
			String a = driver.findElement(By.xpath("//*[@id= 'newEqualToConfirm']")).getText();
			String b = "New password and confirm new password do not match";
			Assert.assertEquals(a, b);
		
		
		
		
		
		}


  @AfterTest
  public void afterTest() {
  }

}
