package TestNGPCKG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class guru99_Gettitle {
		WebDriver driver;
  
		@BeforeTest
		  public void beforeTest() {
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\sony\\Desktop\\Selenium Webdriver\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("http://demo.guru99.com/test/link.html");
			
		
		}
		@Test
  public void Googletitle() {
			driver.findElement(By.xpath("(//a[text()='click here'])[1]")).click();
			String a = driver.getTitle();
			String b = "Google";
			Assert.assertEquals(a, b);
			
  }
		@Test
		  public void facebooktitle() {
			driver.navigate().back();	
			driver.findElement(By.xpath("(//a[text()='click here'])[2]")).click();
			String a = driver.getTitle();
			String b = "Facebook � log in or sign up";
			Assert.assertEquals(a, b);
					
		  }
  

  @AfterTest
  public void afterTest() {
	  	driver.close();
  }

}
