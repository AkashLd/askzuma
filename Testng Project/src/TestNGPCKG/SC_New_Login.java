package TestNGPCKG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class SC_New_Login extends SC_sign{
  
	 @BeforeTest
	  public void beforeTest() {
	  }
	
//	@Test
//  public void f() {
//  }
	 @Test(priority = 0)
	  public void  loginbothvalueblank() throws InterruptedException{
		Thread.sleep(5000);	
		 driver.findElement(By.xpath("//a[@id='login_button']")).click();
			String a=driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[2]/div[1]/span")).getText();
			String b="Please enter email id or mobile number.";
			String c=driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/div[1]/span")).getText();
			String d="Please enter your password.";
			Assert.assertEquals(a, b);
			Assert.assertEquals(c, d);
				
		}
		@Test(priority = 11)
		  public void passwordblank() {
			
			driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("Please enter your password.");
			driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
			String c=driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/div[1]/span")).getText();
			String d="Please enter your password.";
			Assert.assertEquals(c, d);
			
			
		
		}
		@Test (priority = 12)
		  public void emailblank() {
				
			driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).clear();
			driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("AV3iw1");
			driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
			String a=driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[2]/div[1]/span")).getText();
			String b="Please enter email id or mobile number.";
			Assert.assertEquals(a, b);
		
		}
		
		
		
		
		
		@Test (priority = 13)
		  public void validdetails() throws InterruptedException {
			
			driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("akash.dl1994@gmail.com");
			driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id=\"loginModelBox\"]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();
		  }

	
	
 

  @AfterTest
  public void afterTest() {
	  
  }

}
