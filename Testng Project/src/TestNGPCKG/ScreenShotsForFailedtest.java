package TestNGPCKG;

import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

public class ScreenShotsForFailedtest {
	WebDriver driver;
  @Test
  public void Failedtest() {
	  		System.setProperty("webvdriver.chrome.driver", "C:\\Users\\sony\\Desktop\\Selenium Webdriver\\.chromedriver.exe");
	  		driver = new ChromeDriver();
	  		driver.get("https://askzuma.com/");
	  		driver.findElement(By.xpath("(Sign in)[2]")).click();
	  		driver.findElement(By.xpath("//button[@class='button yellow btnSignin']")).click();
	  		String a = driver.findElement(By.xpath("//button[@class='k-icon p-error field-validation-error']")).getText();
	  		String b = "Abcd";
	  		Assert.assertEquals(a, b);
  
  }
  
  @AfterMethod
	  public void takeScreenShotOnFailure(ITestResult testResult) throws IOException { 
			if (testResult.getStatus() == ITestResult.FAILURE) { 
				File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
				FileUtils.copyFile(scrFile, new File("E:\\ScreenShot" + testResult.getName() + "-" 
						+ Arrays.toString(testResult.getParameters()) +  ".jpg"));
  }
  }
}

  
