package TestNGPCKG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Shopclues_login {
	public WebDriver driver;
	@BeforeTest
	  public void beforeTest() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\sony\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.shopclues.com/");
		Thread.sleep(5000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[1]")).click();
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
	  }

	@Test(priority = 0)
  public void  bothvalueblank(){
		driver.findElement(By.id("login_button")).click();
		String a=driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[2]/div[1]/span")).getText();
		String b="Please enter email id or mobile number.";
		String c=driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/div[1]/span")).getText();
		String d="Please enter your password.";
		Assert.assertEquals(a, b);
		Assert.assertEquals(c, d);
			
	}
	@Test(priority = 1)
	  public void passwordblank() {
		
		driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("Please enter your password.");
		driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
		String c=driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/div[1]/span")).getText();
		String d="Please enter your password.";
		Assert.assertEquals(c, d);
		
		
	
	}
	@Test (priority = 2)
	  public void emailblank() {
			
		driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("AV3iw1");
		driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
		String a=driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[2]/div[1]/span")).getText();
		String b="Please enter email id or mobile number.";
		Assert.assertEquals(a, b);
	
	}
	
	
	
	
	
	@Test (priority = 3)
	  public void f() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("akash.dl1994@gmail.com");
		driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"loginModelBox\"]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();
	  }
	
	
	
	@Test(priority = 4)
		public void logout() {
		
		WebElement ele = driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
		Actions act = new Actions(driver);
		act.moveToElement(ele).perform();
		driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[10]/a")).click();
	}
	
	
	
	
	
	
	@AfterTest
  public void afterTest() {
		driver.close();
  }

}
