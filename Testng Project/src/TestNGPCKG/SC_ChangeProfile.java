package TestNGPCKG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class SC_ChangeProfile {
	public WebDriver driver;
  @BeforeTest
	  public void beforeTest() throws InterruptedException {
	  
	  System.setProperty("webdriver.chrome.driver", "C:\\Users\\sony\\Desktop\\Selenium Webdriver\\chromedriver.exe");
	  driver = new ChromeDriver();
	  //driver.manage().window().maximize();
	  driver.get("https://www.shopclues.com/");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[1]")).click();
	  driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("akash.dl1994@gmail.com");
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("AV3iw1");
	  driver.findElement(By.id("login_button")).click();
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//*[@id=\"loginModelBox\"]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();
	  Actions act = new Actions(driver);
	  WebElement a = driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
	  act.moveToElement(a).perform();
	  driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[4]/a")).click();
	  }
  @Test(priority = 0)
  public void allvalueblank() throws InterruptedException {
	  Thread.sleep(5000);
	  driver.findElement(By.name("user_data[firstname]")).clear();
	  driver.findElement(By.name("user_data[lastname]")).clear();
	  driver.findElement(By.name("user_data[phone]")).clear();
	  driver.findElement(By.name("dispatch[profiles.update.$_action]")).click();
	  String a = driver.findElement(By.xpath("//*[@id=\"checkBlank1\"]/span")).getText();
	  String b = "First name cannot be blank";
	  Assert.assertEquals(a, b);
  
  
  }
  
  @Test(priority = 1)
  public void validfirstname() {
	  
	  driver.findElement(By.name("user_data[firstname]")).sendKeys("AL");
	  driver.findElement(By.name("dispatch[profiles.update.$_action]")).click();
	  String a = driver.findElement(By.xpath("//*[@id=\"checkBlank2\"]")).getText();
	  String b = "Last name cannot be blank";
	  Assert.assertEquals(a, b);
	  
  }
  @Test(priority = 2)
  public void Mobilenumberblank() {
	  
	  driver.findElement(By.name("user_data[lastname]")).sendKeys("D");
	  driver.findElement(By.name("dispatch[profiles.update.$_action]")).click();
	  String a = driver.findElement(By.xpath("//*[@id=\"checkBlank3\"]/span")).getText();
	  String b = "Mobile number cannot be blank";
	  Assert.assertEquals(a, b);
	  
  }
  @Test(priority = 3)
  public void Allvalue() {
  
	  driver.findElement(By.name("user_data[phone]")).sendKeys("8160828601");
	  driver.findElement(By.name("dispatch[profiles.update.$_action]")).click();
  
  
  }
  @Test(priority = 4)
  public void updationmessage() {
	  String a = driver.findElement(By.xpath("//*[@id=\"notification_85214fdf7d646b0d64eeb251383996d0\"]/div/div[2]")).getText();
	  String b = "Profile has been updated successfully.";
	  Assert.assertEquals(a, b);
  }
//  @Test
//  public void f() {
//  }
//  @Test
//  public void f() {
//  }
//  @Test
//  public void f() {
//  }
//  @Test
//  public void f() {
//  }
//  @Test
//  public void f() {
//  }

  @AfterTest
  public void afterTest() {
//  
//	  Actions act2 = new Actions(driver);
//	  WebElement b = driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
//	  act2.moveToElement(b).perform();
//	  driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/div/ul/li[10]/a")).click();
//  
  
  
  }

}
