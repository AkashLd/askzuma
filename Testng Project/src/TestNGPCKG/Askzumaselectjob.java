package TestNGPCKG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Askzumaselectjob {
	
	
	WebDriver driver;
	
	@BeforeTest
	  public void beforeTest() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\sony\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://askzuma.com/");
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("scrollBy(0,500)");
		driver.findElement(By.xpath("//span[@id='select2-chosen-1']")).click();
		driver.findElement(By.xpath("//div[@id='select2-result-label-8']")).click();//year
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[@id='select2-chosen-46']")).click();//make
		driver.findElement(By.xpath("//div[@id='select2-result-label-47']")).click();//make
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[@id='select2-chosen-90']")).click();//model
		driver.findElement(By.xpath("//div[@id='select2-result-label-91']")).click();//model
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[@id='select2-chosen-93']")).click();//sm
		driver.findElement(By.xpath("//div[@id='select2-result-label-94']")).click();//sm
		driver.findElement(By.xpath("//button[@id='selectJob']")).click();
		Thread.sleep(5000);
		
	  }
	@Test
  public void errormsg() {
	
		String a = driver.findElement(By.xpath("(//div[@class='message'])[1]")).getText();
		String b ="Please specify the the location.";
		Assert.assertEquals(a, b);
		
  }
  

  @AfterTest
  public void afterTest() {
  
	  	driver.findElement(By.xpath("//button[@class='button green yes']")).click();
	  	driver.close();
  
  }

}
