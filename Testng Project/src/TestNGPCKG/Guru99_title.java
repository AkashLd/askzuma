package TestNGPCKG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Guru99_title {
	
	
	WebDriver driver;
	@BeforeTest
	  public void beforeTest() {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\sony\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://demo.guru99.com/test/newtours/");
		
		
		
		
	  }
	@Test(priority = 0)
  public void Title() {
		String a = driver.getTitle();
		String b = "Welcome: Mercury Tours";
		Assert.assertEquals(a, b);
		
		
		
  }
  @Test(priority = 1)
  public void Text() {
	  	driver.findElement(By.xpath("//a[text()='REGISTER']")).click();
	  	driver.findElement(By.xpath("//input[@name='firstName']")).sendKeys("ad");
	  	driver.findElement(By.xpath("//input[@name='lastName']")).sendKeys("Sn");
	  	driver.findElement(By.xpath("//input[@id='userName']")).sendKeys("user1@test.com");
	  	driver.findElement(By.xpath("//input[@id='email']")).sendKeys("ad");
	  	driver.findElement(By.xpath("//input[@name='password']")).sendKeys("user1");
	  	driver.findElement(By.xpath("//input[@name='confirmPassword']")).sendKeys("user1");
	  	driver.findElement(By.xpath("//input[@name='submit']")).click();
	  	String a = driver.findElement(By.xpath("(//font[@face='Arial, Helvetica, sans-serif'])[3]")).getText();
	  	String b = "Thank you for registering. You may now sign-in using the user name and password you've just entered.";
	  	Assert.assertEquals(a, b);
  
  
  
  
  }
  @Test(priority = 2)
  public void signtext() {
	  	driver.findElement(By.xpath("//a[text()=' sign-in ']")).click();
	  	String a = driver.findElement(By.xpath("/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td/p/font")).getText();
	  	String b = "Welcome back to Mercury Tours! Enter your user information to access the member-only areas of this site. If you don't have a log-in, please fill out the registration form.";
	  	Assert.assertEquals(a, b);
	  	
	  
	  
  }
  @Test(priority = 3)
  public void Logintext() {
	  driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("user1@test.com");
	  driver.findElement(By.xpath("//input[@name='password']")).sendKeys("user1");
	  driver.findElement(By.xpath("//input[@name='submit']")).click();
	  String a = driver.findElement(By.xpath("//*[text()='Login Successfully']")).getText();
	  String b = "Login Successfully";
	  Assert.assertEquals(a, b);
  
  }
  
  @AfterTest
  public void afterTest() {
	  driver.findElement(By.xpath("//a[text()='SIGN-OFF']")).click();
	  driver.close();
  }

}
