package TestNGPCKG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class SC_ChangePassword {
	public WebDriver driver;
	@BeforeTest
	  public void beforeTest() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\sony\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		  driver = new ChromeDriver();
		  //driver.manage().window().maximize();
		  driver.get("https://www.shopclues.com/");
		  driver.manage().window().maximize();
		  Thread.sleep(5000);
		  driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[1]")).click();
		  driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
		  Thread.sleep(5000);
		  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("akash.dl1994@gmail.com");
		  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("AV3iw1");
		  driver.findElement(By.id("login_button")).click();
		  Thread.sleep(5000);
		  driver.findElement(By.xpath("//*[@id=\"loginModelBox\"]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();
		  Actions act = new Actions(driver);
		  WebElement a = driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
		  act.moveToElement(a).perform();
		  driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[4]/a")).click();
		  Thread.sleep(5000);
		  driver.findElement(By.xpath("//a[text() = 'Change Password']")).click();
		  
	  }
	@Test(priority = 0)
  public void allblank() {
		
		driver.findElement(By.xpath("//input[@id= 'save_profile_but']")).click();
		String a = driver.findElement(By.xpath("//*[text()='Current password cannot be empty']")).getText();
		String b = "Current password cannot be empty";
		Assert.assertEquals(a, b);
		
	
	
	}
	@Test(priority = 1)
	  public void Cp() {
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("AV3iw1");
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		String a = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]/span")).getText();
		String b = "New password cannot be empty";
		Assert.assertEquals(a, b);
	  }
	@Test(priority = 2)
	  public void invalidnewpassword() {
		driver.findElement(By.xpath("//input[@id= 'password1']")).sendKeys("123456");
		driver.findElement(By.xpath("//input[@id= 'save_profile_but']")).click();
		String a = driver.findElement(By.xpath("//div[@id= 'password1Blank']")).getText();
		String b = "Password must be alphanumeric";
		Assert.assertEquals(a, b);
	
	
	}
	@Test(priority = 3) 
	  public void blankretypepassword() {
		
		driver.findElement(By.xpath("//input[@id= 'password1']")).clear();
		driver.findElement(By.xpath("//input[@id= 'password1']")).sendKeys("A123456");
		driver.findElement(By.xpath("//input[@id= 'save_profile_but']")).click();
		String a = driver.findElement(By.xpath("//*[text()= 'Confirm password cannot be empty']")).getText();
		String b = "Confirm password cannot be empty";
		Assert.assertEquals(a, b);
	
	
	
	}
	@Test(priority = 4)
	  public void mismatchpassword() {
		driver.findElement(By.xpath("//*[@id= 'password2']")).sendKeys("654321A");
		driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
		String a = driver.findElement(By.xpath("//*[@id= 'newEqualToConfirm']")).getText();
		String b = "New password and confirm new password do not match";
		Assert.assertEquals(a, b);
	
	
	
	
	
	}
//	@Test
//	  public void f() {
//	  }
//	@Test
//	  public void f() {
//	  }

  @AfterTest
  public void afterTest() {
  }

}
